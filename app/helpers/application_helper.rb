module ApplicationHelper

  def main_menu
    { "/schedule" => "Schedule" ,
      "/accommodation" => "Accommodation" ,
      "/tickets" => "Tickets" ,
       "/location" => "Location" ,
       "/faq" => "FAQ" ,
       "/contact" => "Contact" ,
     }
  end

  def mobile_menu
    main_menu
  end

end
