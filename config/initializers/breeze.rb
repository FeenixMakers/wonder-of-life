require "breeze"

# directory inside /app/assets/images where YOUR images are kept
# if you change this and add own styles, you will still need a breeze directory
#  for the previews (card_preview and section_preview)
Breeze.images_dir = "breeze"

# directory where data and styles are kept
# Notice that the data is ALWAYS inside a breeze directory,
# so in the default case Rails.root/breeze/*.yml
Breeze.data_dir = "."


# text colors, keys are options shown to user, values what gets replaced
Breeze.text_color =  { "white" => "text-white",
          "none" => "",
          "pink" => "text-pink-500" ,
          "blue" => "text-cyan-400",
          "green" => "text-lime-200",
          "caladon" => "text-emerald-200",
          "orange" => "text-amber-400",
          "light_gray" => "text-gray-100",
          "solid_black" => "text-slate-800",
        }

# margin option, keys are options shown to user, values what gets replaced
Breeze.margin =  { "none" =>  "m-0",
              "small" => " m-2 md:m-4  lg:6  xl:m-8",
              "medium" => "m-5 md:m-8  lg:10 xl:m-14",
              "large" => " m-8 md:m-12 lg:16 xl:m-20",}

# background colors
Breeze.background = {"white" =>  "bg-white",
                  "none" =>  "",
                  "pink" => "bg-pink-500",
                  "blue" => "bg-cyan-400",
                  "green" => "bg-lime-200",
                  "caladon" => "bg-emerald-200",
                  "orange" => "bg-amber-400",
                  "light_gray" => "bg-gray-100",
                  "solid_black" => "bg-slate-900  text-white",  }

# shade options
Breeze.shade_color =  {"white_25" => "bg-white/25",
                    "none" => "",
                    "black_25" => "bg-black/25",
                    "light_blue_25" => "bg-cyan-100/25",
                    "light_red_25" => "bg-orange-300/25",
                    "solid_blue_25" => "bg-cyan-700/25",
                    "solid_red_25" => "bg-orange-800/25", }

# amount of text columns
Breeze.text_columns = {
      "3" => "columns-1 md:columns-2 lg:columns-3",
      "4" => "columns-1 md:columns-2 lg:columns-3 xl:columns-4",
      "2" => "columns-1 md:columns-2" }

Breeze.columns = { "1" => "grid-cols-1",
                "2" => "grid-cols-1 md:grid-cols-2" ,
                "3" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-3",
                "4" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-4",
                "5" => "grid-cols-1 md:grid-cols-3 lg:grid-cols-5",
                "6" => "grid-cols-2 md:grid-cols-4 lg:grid-cols-6", }
